# Faceswap API

## How to run ?

- Pre-setup (Cài đặt git và git lfs)

```bash
apt update
apt install git git-lfs
```

- Clone source and checkpoints (Download source)
```bash
git clone https://gitlab.com/namnh-dev/faceswap
cd faceswap
git-lfs pull
```

- Install requirements (Cài đặt thư viện)
```bash
pip install -r requirements.txt
```

- Add your AUTH_TOKEN to .env file (Tạo auth token -> Thay đổi <your_token> thành mã bất kỳ)
```bash
sed -i 's/TOKEN=.*/TOKEN=<your_token>/' .env
```

- Deploy API (Thay <PORT> thành port bất kỳ của server, <NUM_WORKERS> là 2 với server có 16GB VRAM GPU)
```
uvicorn main:app --host 0.0.0.0 --port <PORT> --workers <NUM_WORKERS> 
```


## How to use ?

### Postman documents 
`https://documenter.getpostman.com/view/20660396/2sA2xpSUsZ`

### API Request (Method 1: Use GET)
- Enpoint: `/v1/faceswap`
- Method: `GET`
- Params:
    - `source_url`
    - `target_url`

- Full URL Endpoint: `/v1/faceswap?source_url=<SOURCE_URL>&target_url=<TARGET_URL>`

### API Schema (Method 2: Use POST)

- Endpoint: `/v1/faceswap`
- Method: `POST` 
- Body: `form-data`
    - `source_image`
    - `target_image`
- Header: `Bearer Token`


### Example cURL
```
curl --location '<HOST>/v1/faceswap' \
--header 'Authorization: Bearer admin@password123' \
--header 'Cookie: __cflb=02DiuD2jGpGXwi6JyhrMvdna4L7jQMWfGj12WME3voHWt' \
--form 'source_image=@"/D:/Downloads/b76c4491-0938-46ad-b476-c3e3c6554366.jpg"' \
--form 'target_image=@"/D:/Downloads/Duong_Lam_2020.jpg"'
```
