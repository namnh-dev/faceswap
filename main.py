import io
import os
import base64

from loggers import logger
from dotenv import load_dotenv

from fastapi import FastAPI, Query, HTTPException, Header, Form, UploadFile, File
from fastapi.responses import StreamingResponse
from predict import Predictor

from PIL import Image

load_dotenv()

app = FastAPI(
    title="Face Swap Service",
    docs_url="/api_docs",
)

_TOKEN = os.environ.get("TOKEN")
if not _TOKEN:
    raise Exception("Please set TOKEN environment variable")

predictor = Predictor()

async def verify_token(authorization):
    token = authorization.split("Bearer ")[-1].strip()
    return token == _TOKEN

@app.get("/ping")
async def ping():
    return {"status": "OK"}

@app.get("/v1/faceswap")
async def process_facewap_image_url(
    source_url: str = Query(...),
    target_url: str = Query(...),
    enhance: bool = Query(True),
    authorization: str = Header(None)
):
    image_result = await predictor.predict(source_url=source_url, target_url=target_url, enhance=enhance)
    
    if image_result:
        buf = io.BytesIO()
        image_result.save(buf, format='PNG')
        img_byte = buf.getvalue()
        img_base64 = base64.b64encode(img_byte)
        img_base64_str = img_base64.decode('utf-8')
        # return {"base64_image": img_base64_str}
        # Update
        return dict(
            success=True,
            result=f"data:image/jpeg;base64,{img_base64_str}"
        )
    else:
        raise HTTPException(status_code=202, detail="No face detected")
    
@app.post("/v1/faceswap")
async def process_facewap_image(
    source_image: UploadFile = File(...),
    target_image: UploadFile = File(...),
    enhance: bool = Form(True),
    authorization: str = Header(...)
):
    verified = await verify_token(authorization)
    if not verified:
        raise HTTPException(status_code=401, detail="Unauthorized")

    # Read the content of the uploaded file
    content_source = await source_image.read()
    content_target = await target_image.read()
    
    # Convert the binary content to a PIL Image
    source_image = Image.open(io.BytesIO(content_source)).convert("RGB")
    target_image = Image.open(io.BytesIO(content_target)).convert("RGB")
    
    image_result = await predictor.run(source_image=source_image, target_image=target_image, enhance=enhance)
    
    if image_result:
        buf = io.BytesIO()
        image_result.save(buf, format='JPEG')
        buf.seek(0)      
        return StreamingResponse(buf, media_type="image/jpeg")
    else:
        raise HTTPException(status_code=202, detail="No face detected")