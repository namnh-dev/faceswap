
import aiohttp 
import asyncio
import insightface
import onnxruntime
from insightface.app import FaceAnalysis
import gfpgan
import time
from typing import Any
from loggers import logger
from PIL import Image
from io import BytesIO
import traceback
import numpy as np


class Predictor():
    def __init__(self):
        self.det_thresh = 0.1
        self.setup()
        
    def setup(self):
        self.face_swapper = insightface.model_zoo.get_model('checkpoints/inswapper_128.onnx', providers=onnxruntime.get_available_providers())
        self.face_enhancer = gfpgan.GFPGANer(model_path='checkpoints/GFPGANv1.4.pth', upscale=1)
        self.face_analyser = FaceAnalysis(name='buffalo_l', root='checkpoints')

    def get_face(self, img_data, image_type='target'):
        try:
            logger.info(self.det_thresh)
            self.face_analyser.prepare(ctx_id=0, det_thresh=0.5)
            if image_type == 'source':
                self.face_analyser.prepare(ctx_id=0, det_thresh=self.det_thresh)
            analysed = self.face_analyser.get(img_data)
            logger.info(f'face num: {len(analysed)}')
            if len(analysed) == 0:
                msg = 'no face'
                logger.error(msg)
                raise Exception(msg)
            largest = max(analysed, key=lambda x: (x.bbox[2] - x.bbox[0]) * (x.bbox[3] - x.bbox[1]))
            return largest
        except Exception as e:
            logger.error(str(e))
            raise Exception(str(e))

    def enhance_face(self, target_face, target_frame, weight=0.5):
        start_x, start_y, end_x, end_y = map(int, target_face['bbox'])
        padding_x = int((end_x - start_x) * 0.5)
        padding_y = int((end_y - start_y) * 0.5)
        start_x = max(0, start_x - padding_x)
        start_y = max(0, start_y - padding_y)
        end_x = max(0, end_x + padding_x)
        end_y = max(0, end_y + padding_y)
        temp_face = target_frame[start_y:end_y, start_x:end_x]
        if temp_face.size:
            _, _, temp_face = self.face_enhancer.enhance(
                temp_face,
                paste_back=True,
                weight=weight
            )
            target_frame[start_y:end_y, start_x:end_x] = temp_face
        return target_frame

    async def download_image(self, url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                if response.status == 200:
                    image_data = await response.read()
                    image_bytes = BytesIO(image_data)
                    image = Image.open(image_bytes).convert("RGB")
                    return image
                else:
                    return None

    async def predict(
        self,
        source_url: str,
        target_url: str,
        weight: float=1.0,
        enhance: bool=True,
    ) -> Any:
        
        """Run a single prediction on the model"""
        try:
            t0 = time.time()
            source_image, target_image = await asyncio.gather(
                self.download_image(source_url),
                self.download_image(target_url)
            )
            
            t1 = time.time()
            logger.info(f'download image time: {(t1 - t0) * 1000} ms')   
            if not (source_image and target_image):
                logger.error(f"download image failed, source_url: {source_url}, target_url: {target_url}")
                return None
            return await self.run(source_image, target_image, weight, enhance)
        
        except Exception as e:
            logger.error(traceback.format_exc())
            return None
    
    async def run(
        self,
        source_image: Image,
        target_image: Image,
        weight: float=1.0,
        enhance: bool=True,
    ) -> Any:
        try:
            source_frame = np.array(source_image)[:, :, ::-1]
            frame = np.array(target_image)[:, :, ::-1]
            
            t1 = time.time()
            source_face = self.get_face(source_frame, image_type='source')
            target_face = self.get_face(frame)
            
            t2 = time.time()
            result = self.face_swapper.get(frame, target_face, source_face, paste_back=True)
            
            t3 = time.time()
            if enhance:
                result = self.enhance_face(target_face, result, weight)
            t4 = time.time()
            
            logger.info(f'Time lists {[(t2 - t1) * 1000, (t3 - t2) * 1000, (t4 - t3) * 1000]} ')
            logger.info(f'Total time process AI: {(t3 - t1) * 1000} ms')

            result = result[:, :, ::-1]
            return Image.fromarray(result).convert('RGB')
        
        except Exception as e:
            logger.error(traceback.format_exc())
            return None